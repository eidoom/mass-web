export async function load({ fetch }) {
  const key = "password";

  const response = await fetch(`http://127.0.0.1:8000/list_masses?key=${key}`);

  const data = await response.json();

  for (const row of data) {
    row[0] = new Date(1000 * row[0]);
  }

  return { masses: data };
}
